var websocketonline;
var websocketnoti;

window.onload = function() { // execute once the page loads
	initialize();
	document.getElementById("chat").focus();
}

function initialize() { // URI = ws://10.16.0.165:8080/chat/chat
	connectOnline('ws://' + window.location.host + '/sd/OnlineWebSocketServlet');
	connectNoti('ws://' + window.location.host + '/sd/NotificationWebSocketServlet');
}

function connectOnline(host) { // connect to the host websocket servlet
	if ('WebSocket' in window)
		websocketonline = new WebSocket(host);
	else if ('MozWebSocket' in window)
		websocketonline = new MozWebSocket(host);
	else {
		writeToOnline('Get a real browser which supports WebSocket.');
		return;
	}

	websocketonline.onopen    = onOpenOnline; // set the event listeners below
	websocketonline.onclose   = onCloseOnline;
	websocketonline.onmessage = onMessageOnline;
	websocketonline.onerror   = onErrorOnline;
}

function connectNoti(host) { // connect to the host websocket servlet
	if ('WebSocket' in window)
		websocketnoti = new WebSocket(host);
	else if ('MozWebSocket' in window)
		websocketnoti = new MozWebSocket(host);
	else {
		writeToNoti('Get a real browser which supports WebSocket.');
		return;
	}

	websocketnoti.onopen    = onOpenNoti; // set the event listeners below
	websocketnoti.onclose   = onCloseNoti;
	websocketnoti.onmessage = onMessageNoti;
	websocketnoti.onerror   = onErrorNoti;
}


window.onunload=function(){
	websocketonline.close();
	websocketnoti.close();
}

function onOpenOnline(event) {
	var message = "get.users";
	websocketonline.send(message);
}

function onOpenNoti(event){
}

function onCloseOnline(event) {
	websocketonline.close();
}

function onCloseNoti(event){
	websocketonline.close();
}

function onMessageOnline(message) { // print the received message
	writeToOnline(message.data);
}

function onMessageNoti(message){
	writeToNoti(message.data);
}

function onErrorOnline(event) {
}

function onErrorNoti(event){
}

function writeToOnline(text) {
	var online = document.getElementById('online');
	var p = document.createElement('p');
	p.style.wordWrap='break-word';
	
	while (online.childNodes.length > 0)
		online.removeChild(online.firstChild);

	p.innerHTML=text;
	online.appendChild(p);
}

function writeToNoti(text){
	var notification = document.getElementById('notification');
	var p = document.createElement('p');
	p.style.wordWrap='break-word';
	p.innerHTML=text;
	notification.appendChild(p);
	
	while (notification.childNodes.length > 5)
		notification.removeChild(notification.firstChild);

}