package db;

import rmi.ClientInterface;
import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import db.FacebookRestClient;

public class User implements Serializable{
	static final long serialVersionUID = 1L;
	private ConcurrentLinkedQueue<User> friends;
	private ConcurrentLinkedQueue<User> followers;
	private ConcurrentLinkedQueue<Post> posts;
	private ConcurrentLinkedQueue<Message> messages;
	private Date date;
	private String name;
	private String pass;
	transient private ConcurrentLinkedQueue<ObjectOutputStream> tcpOut;
	transient private ConcurrentLinkedQueue<ClientInterface> rmiOut;
	FacebookRestClient fbc;
	private int nLogged;
	private int lastPostID;
	private int lastMsgID;

	public User(String name,String password,ObjectOutputStream out){
		this.date = new Date();
		this.friends = new ConcurrentLinkedQueue<>();
		this.followers = new ConcurrentLinkedQueue<>();
		this.posts = new ConcurrentLinkedQueue<>();
		this.messages = new ConcurrentLinkedQueue<>();
		this.name = name;
		this.pass = password;
		this.tcpOut = new ConcurrentLinkedQueue<>();
		this.rmiOut = new ConcurrentLinkedQueue<>();
		this.nLogged = 0;
		this.lastPostID = 0;
		this.fbc = null;
	}

	public void addFriend(User friend){
		this.friends.offer(friend);
	}
	public void addFollower(User follower){
		this.followers.offer(follower);
	}
	public void addPost(Post post){
		post.setID(++lastPostID);
		this.posts.offer(post);
	}
	public void addMessage(Message message){
		message.setID(++lastMsgID);
		this.messages.offer(message);
	}
	public void setDate(Date date){
		this.date = date;
	}
	public void setName(String name){
		this.name = name;
	}
	public void setPass(String pass){
		this.pass = pass;
	}
	public void setStream(ObjectOutputStream out){
		this.tcpOut.offer(out);
	}
	public void startStreams(){
		this.tcpOut = new ConcurrentLinkedQueue<>();
		this.rmiOut = new ConcurrentLinkedQueue<>();
		this.nLogged = 0;
	}
	public void addRMI(ClientInterface c){
		rmiOut.offer(c);
	}
	public void removeRMI(ClientInterface c){
		try{
			rmiOut.remove(c);
		} catch (Exception e){
			e.printStackTrace();
		}
	}
	public void removeStream(ObjectOutputStream out){
		try{
			this.tcpOut.remove(out);
		} catch (Exception e){
			e.printStackTrace();
		}
	}
	public void setLogged(Boolean tmp){
		if(tmp){
			nLogged++;
		}
		else{
			nLogged--;
		}
	}

	public ConcurrentLinkedQueue<User> getFriends(){
		return this.friends;
	}
	public ConcurrentLinkedQueue<User> getFollowers(){
		return this.followers;
	}
	public ConcurrentLinkedQueue<Post> getPosts(){
		return this.posts;
	}
	public ConcurrentLinkedQueue<Message> getMessages(){
		return this.messages;
	}
	public Date getDate(){
		return this.date;
	}
	public String getName(){
		return this.name;
	}
	public String getPass(){
		return this.pass;
	}
	public ConcurrentLinkedQueue<ObjectOutputStream> getStream(){
		return this.tcpOut;
	}
	public int getLastPostID(){
		return this.lastPostID;
	}
	public Boolean getLogged(){
		if(nLogged==0) return false;
		return true;
	}
	public ConcurrentLinkedQueue<ClientInterface> getClients(){
		return this.rmiOut;
	}
	public Post findPost(int id){
		Iterator<Post> it = posts.iterator();
		Post tmp;
		while(it.hasNext()){
			tmp = it.next();
			if(tmp.getID() == id){
				return tmp;	
			}
		}
		return null;
	}
	public void removePost(Post tmp){
		posts.remove(tmp);
	}

	public Boolean isFriend(User user){
		Iterator<User> it = this.friends.iterator();
		User tmp;
		while(it.hasNext()){
			tmp = it.next();
			if (tmp == user) return true;
		}
		return false;
	}
	public User findFriend(String name){
		Iterator<User> it = this.friends.iterator();
		User tmp;
		while(it.hasNext()){
			tmp = it.next();
			if(tmp.getName().compareTo(name)==0){
				return tmp;	
			}
		}
		return null;	
	}
	public void removeFriend(User tmp){
		friends.remove(tmp);
	}
	public void removeFollower(User tmp){
		followers.remove(tmp);
	}

	public FacebookRestClient getFacebookClient(){
		return this.fbc;
	}

	public void setFacebookRestClient(FacebookRestClient temp){
		this.fbc = temp;
	}

	public ArrayList<Post> updateFacebookPosts(){
		ArrayList<Post> psts = new ArrayList<Post>();
		if( this.fbc != null){
			ArrayList<String> temp = new ArrayList<String>(); 
			ArrayList<String> info = new ArrayList<String>();
			//Primeiro iniciar a busca a um - Retirar post mais recente
			int index = 1;
			temp = this.fbc.getLatestPost(index);

			//Enquanto que o post retirado tiver um time maior do que aquele que conta como ultimo
			System.out.println("comparing " + fbc.getlastPostID() + " with " + temp.get(2));
			while(Integer.parseInt(fbc.getlastPostID()) < Integer.parseInt(temp.get(2))){
				System.out.println("Encontrei 1 post mais recente!");
				if(info.size()>0){
					System.out.println("Já tenho alguns adicionados no array!");
					if(Integer.parseInt(temp.get(2)) >= Integer.parseInt(info.get(info.size()-1))){
						System.out.println("Ups este já existe nao o vou adicionar!");
						index++;
						continue;
					}
				}
				
				info.add(temp.get(0));
				info.add(temp.get(1));
				info.add(temp.get(2));
				//incrementar index
				index++;
				temp = fbc.getLatestPost(index);
			}
			System.out.println("Sai do loop");

			if(info.size()>0){
				this.fbc.setlastPostID(info.get(2));
				System.out.println("Adcionar post mais recente como " + temp.get(2));
			}

			//adiconar os posts pela ordem inversa
			for(int i=info.size()-1;i>0;i--){
				//System.out.println("Novo Post a adiconar! id: " + info.get(i-2) + " commment: " +  info.get(i-1) );
				Post pst = new Post("Facebook Post - " + info.get(i-2), info.get(i-1), new Date());
				pst.setFbPostId(info.get(i-2));
				psts.add(pst);
				i = i-2;
			}

			//verificar se estes posts já existem no user
			for(int i=0;i<psts.size();i++){
				Post pst = psts.get(i);
				Iterator<Post> it;
				Post tmp;
				it = posts.iterator();

				while(it.hasNext()){
					tmp = it.next();
					if(tmp.getFbPostId() != null){
						if(tmp.getFbPostId().equals(pst.getFbPostId())){
							psts.remove(i);
							break;
						}
					}
				}
			}
		}
		return psts;
	}
}
