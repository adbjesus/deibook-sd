package db;

import java.io.*;
import java.util.*;

public class Reply implements Serializable {
	static final long serialVersionUID = 4L;
	private String title;
	private Boolean bool;
	private User user;
	private Post post;
	private int orderID;
	private Message message;
	private String[] users;
	private Post[] posts;
	private Message[] messages;
	private String report;
	private byte[] imageBytes;

	public void setTitle(String title){
		this.title = title;
	}
	public void setOrderID(int id){
		this.orderID = id;
	}
	public void setBool(Boolean bool){
		this.bool = bool;
	}
	public void setUser(User user){
		this.user = user;
	}
	public void setPost(Post post){
		this.post = post;
	}
	public void setMessage(Message message){
		this.message = message;
	}
	public void setUsers(String[] users){
		this.users = users;
	}
	public void setPosts(Post[] posts){
		this.posts = posts;
	}
	public void setMessages(Message[] messages){
		this.messages = messages;
	}
	public void setReport(String report){
		this.report = report;
	}
	public void setImageBytes(byte[] imageBytes){
		this.imageBytes = imageBytes;
	}

	public int getOrderID(){
		return this.orderID;
	}
	public String getTitle(){
		return this.title;
	}
	public Boolean getBool(){
		return this.bool;
	}
	public User getUser(){
		return this.user;
	}
	public Post getPost(){
		return this.post;
	}
	public Message getMessage(){
		return this.message;
	}
	public String[] getUsers(){
		return this.users;
	}
	public Post[] getPosts(){
		return this.posts;
	}
	public Message[] getMessages(){
		return this.messages;
	}
	public String getReport(){
		return this.report;
	}
	public byte[] getImageBytes(){
		return this.imageBytes;
	}
	

}