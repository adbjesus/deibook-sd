package db;

import java.io.UnsupportedEncodingException;
import java.util.*;
import java.io.*;

import org.scribe.builder.*;
import org.scribe.builder.api.*;
import org.scribe.model.*;
import org.scribe.oauth.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.net.URLEncoder;
import java.lang.String;
import java.io.StringReader;

public class FacebookRestClient implements Serializable{
	public static String apiKey = "460615943974727";
    public static String apiSecret = "f884f3c3f91466278d5547eb225ad455";
	/*atributos da classe*/
	private Token empty_token = null;
	transient private Verifier verifier;
	private Token accessToken;
	private String facebookURL = "https://api.facebook.com/method/";
	transient private OAuthService service;
	private String lastPostID;
	/*construtor*/
	public FacebookRestClient(Boolean web){
		newService(web);
	}
	/*metodos da classe*/

	public void newService(Boolean web){
		String web_string = "";
		if(web){
			web_string = "?redirect=http://localhost:8080/sd/FacebookLogin";
		}
		System.out.println(web_string);
		this.service = new ServiceBuilder()
                  		.provider(FacebookApi.class)
	                    .apiKey(FacebookRestClient.apiKey)
	                    .apiSecret(FacebookRestClient.apiSecret)
	                    .callback("http://eden.dei.uc.pt/~amaf/echo.php"+web_string) // Do not change this.
	                    .scope("publish_stream,read_stream")
	                    .build();
	}

	public String getlastPostID(){
		return this.lastPostID;
	}
	public void setlastPostID(String temp){
		this.lastPostID = temp;
	}
	public String getAuthorizationURL(){
		String authorizationUrl = service.getAuthorizationUrl(empty_token);
		return authorizationUrl;
	}

	public void verifyToken(String authToken){
		verifier = new Verifier(authToken);
		// Trade the Request Token and Verifier for the Access Token
		accessToken = service.getAccessToken(empty_token, verifier);
		System.out.println("Teste3");
		//retirar o id do ultimo post adicionado
		ArrayList<String> tmp = new ArrayList<String>();
		tmp = getLatestPost(1);
		System.out.println("Setting last post time as: " + tmp.get(2));
		this.lastPostID = tmp.get(2);
	}


	public String addFacebookPost(String message){
		String url="";

		try {
			url = facebookURL +  "stream.publish?message=" + URLEncoder.encode(message, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		OAuthRequest request = new OAuthRequest(Verb.PUT,url);
		service.signRequest(accessToken,request);
	    Response response = request.send();

	    return getXMLResponse("stream_publish_response",response.getBody());
	}

	public void addFacebookComment(String id,String message){
		String url="";

		try {
			url = facebookURL +  "stream.addComment?post_id=" + id + "&comment=" + URLEncoder.encode(message, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		OAuthRequest request = new OAuthRequest(Verb.PUT,url);
		service.signRequest(accessToken,request);
	    Response response = request.send();
	}

	public Boolean removeFacebookPost(String id){
		String url = facebookURL +  "stream.remove?post_id=" + id;
		
		OAuthRequest request = new OAuthRequest(Verb.DELETE,url);
		service.signRequest(accessToken,request);
	    Response response = request.send();

	    //Fazer parsing e retornar true ou false
	    if(getXMLResponse("stream_remove_response",response.getBody()).equals("1")){
	    	return true;
	    }else{
	    	return false;
	    }
	}

	private String getXMLResponse(String field,String body){
		try {
	  		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
           	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
           	Document doc = dBuilder.parse(new InputSource(new StringReader(body)));
           	doc.getDocumentElement().normalize();
           
           	NodeList nodes = doc.getElementsByTagName(field).item(0).getChildNodes();
            Node node = (Node) nodes.item(0);
            System.out.println(node.getNodeValue().toString());
            return node.getNodeValue().toString();
	    } catch (Exception ex) {
	    	if(field.equals("stream_remove_response")){
	    		System.out.println("Nao pode apagar esse post.");
	    	}else{
	    		ex.printStackTrace();
	    	}
	    	return "";
	    }
	}

	public ArrayList<String> getLatestPost(int index){
		System.out.println("index:" + Integer.toString(index));
		ArrayList<String> info = new ArrayList<String>();

		OAuthRequest request = new OAuthRequest(Verb.GET, facebookURL + "status.get?limit=" + Integer.toString(index));
   		service.signRequest(accessToken,request);
    	Response response = request.send();

    	try {
	    	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	    	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	    	Document doc = dBuilder.parse(new InputSource(new StringReader(response.getBody())));
	    	doc.getDocumentElement().normalize();
	    	
			
	    	NodeList nodes = doc.getElementsByTagName("user_status");

	    	for (int i = 0; i < nodes.getLength(); i++) {
		    	Node node = nodes.item(i);
		    	if (node.getNodeType() == Node.ELEMENT_NODE) {
			    	Element element = (Element) node;
			    	//System.out.println("facebook_id: " + getValue("uid", element) + "_" + getValue("status_id", element));
			    	//System.out.println("message: " + getValue("message", element));
			    	if(i==index-1){
			    		info.add(getValue("uid", element) + "_" + getValue("status_id", element));
			    		info.add(getValue("message", element));
			    		info.add(getValue("time",element));
			    	}
		    	}
	    	}
	    } catch (Exception ex) {
	    	ex.printStackTrace();
	    }

	    return info;
	}

	private static String getValue(String tag, Element element) {
  		NodeList nodes = element.getElementsByTagName(tag).item(0).getChildNodes();
  		Node node = (Node) nodes.item(0);
  		return node.getNodeValue();
	}	
}