package bean;

import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.*;
import java.util.*;

import db.User;
import rmi.*;
import servlets.*;

public class Chatrooms
extends java.rmi.server.UnicastRemoteObject
implements WebServerInterface {
	private ArrayList<String> chatrooms;
	private static ArrayList<User> onlineUsers = new ArrayList<User>();
	private String rmiUrl = null;
	private RMIServerInterface server = null;

	public Chatrooms() throws java.rmi.RemoteException{
		chatrooms = new ArrayList <String>();
		this.rmiUrl = "rmi://localhost:8000/rmiServer";
		try{
			this.server = (RMIServerInterface) Naming.lookup(this.rmiUrl);
			this.server.subscribeWS(this);
		} catch (Exception e){
			e.printStackTrace();
		}	
	}

	public void addRoom(String nome){
		chatrooms.add(nome);
	}

	public void removeRoom(String nome){
		chatrooms.remove(nome);
	}
	
	public boolean hasRoom(String name){
		return chatrooms.contains(name);
	}

	public ArrayList<String> getRooms(){
		return this.chatrooms;
	}

	public void addOnlineUser(User user) throws java.rmi.RemoteException{
		Chatrooms.onlineUsers.add(user);
		Chatrooms.updateUsers();
	}

	public void removeOnlineUser(User user) throws java.rmi.RemoteException{
		for(int i=0;i<Chatrooms.onlineUsers.size();i++){
			if(user.getName().compareTo(Chatrooms.onlineUsers.get(i).getName())==0){
				Chatrooms.onlineUsers.remove(i);
				Chatrooms.updateUsers();
				break;
			}
		}
		
	}

	public static void updateUsers(){
		String users = "";

		for(int i=0;i<onlineUsers.size();i++){
			users += onlineUsers.get(i).getName()+"<br>";
		}
		System.out.println(users);
		OnlineWebSocketServlet.sendBroadcast(users);
	}


}
