package bean;

public class RMIClient{

	private String rmiURL;

	public RMIClient(){
		rmiURL = "rmi://localhost:8000/rmiServer";
	}
	
	public String getRmiURL(){
		return this.rmiURL;
	}
}
