package bean;

import db.Request;
import db.Reply;
import rmi.RMIServerInterface;
import rmi.ClientInterface;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.*;
import java.util.*;
import java.io.*;
import java.nio.file.Files;

import servlets.OnlineWebSocketServlet;
import servlets.NotificationWebSocketServlet;

import org.apache.catalina.websocket.MessageInbound;

import db.*;

public class Functions 
extends java.rmi.server.UnicastRemoteObject
implements ClientInterface{

	private String rmiUrl = null;
	private RMIServerInterface server = null;
	private String username = null;
	private String chatroom = null;
	private Message[] msgs = null;
	private int msgID;
	private String msgSender;
	private String msgBody;
	private String msgTitle;
	private String[] friends = null;
	private Post[] posts = null;
	private ArrayList<Post> allPosts = null;
	private Post viewPost = null;
	private FacebookRestClient fbc;

	public Functions() throws java.rmi.RemoteException{
		this.username=null;
		this.rmiUrl = "rmi://localhost:8000/rmiServer";
		try{
			this.server = (RMIServerInterface) Naming.lookup(this.rmiUrl);
			this.server.subscribe(this);
		} catch (Exception e){
			e.printStackTrace();
		}
	}

	public Boolean register(String username, String password){
		Boolean ret = false;
		Request req = new Request();
		req.setName(username);
		req.setPass(password);
		req.setUsername(username);
		req.setClientInterface(this);

		try{
			Reply rep = this.server.registar(req);
			System.out.println("LALA");
			ret = rep.getBool();
		} catch (Exception e){
			e.printStackTrace();
		}

		return ret;
	}

	public void setRoom(String chatroom){
		this.chatroom = chatroom;
	}

	public String getRoom(){
		return this.chatroom;
	}

	public void setFacebookRestClient(FacebookRestClient fb){
		Request req = new Request();
		req.setFacebookRestClient(fb);
		req.setUsername(this.username);
		try{
			this.server.setFacebookRestClient(req);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public String[] getFriends(){
		return this.friends;
	}
	
	public String getUsername(){
		return this.username;
	}
	
	public Message[] getMsgs(){
		return this.msgs;
	}

	public void setUsername(String username){
		this.username = username;
	}
	
	public void setMsgID(int msgID){
		this.msgID = msgID;
	}

	public int getMsgID(){
		return this.msgID;
	}

	public Boolean login(String username,String password){
		Boolean ret=false;
		Request req = new Request();
		req.setName(username);
		req.setPass(password);
		req.setUsername(username);
		req.setClientInterface(this);
		
		try{
			Reply rep = this.server.login(req);
			ret = rep.getBool();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return ret;
	}
	
	public Boolean logout() {
		Request req = new Request();
		req.setUsername(username);
		req.setClientInterface(this);
		try{
			Reply rep = this.server.logout(req);
			return rep.getBool();
		} catch (Exception e){
			return false;
		}
	}

	public void sendMessage(String receiver, String tema,String message){
		Request req = new Request();
		req.setMessage(new Message(tema,message,new Date()));
		req.setName(receiver);
		req.setUsername(username);
		req.setClientInterface(this);
		
		try{
			this.server.enviarMensagem(req);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void getInbox(){
		Request req = new Request();
		req.setUsername(username);
		req.setClientInterface(this);
		
		try{
			Reply rep = this.server.consultarMensagens(req);
			this.msgs = rep.getMessages();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public Boolean addFriend(String friend){
		Request req = new Request();
		req.setName(friend);
		req.setUsername(username);
		req.setClientInterface(this);
		boolean sucess = false;
		
		try{
			Reply rep = this.server.adicionarAmigo(req);
			sucess = rep.getBool();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return sucess;
	}
	
	public void removeFriend(String friend){
		Request req= new Request();
		req.setName(friend);
		req.setUsername(username);
		req.setClientInterface(this);
		
		try{
			this.server.removerAmigo(req);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void listFriends(){
		Request req= new Request();
		req.setUsername(username);
		req.setClientInterface(this);
		
		try{
			Reply rep = this.server.listarAmigos(req);
			this.friends = rep.getUsers();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public Message getMsg(int i){
		return this.msgs[i];
	}

	public String getMsgSender(){
		this.msgSender = getMsg(this.msgID).getSender();
		return this.msgSender;
	}

	public String getMsgTitle(){
		this.msgTitle = getMsg(this.msgID).getTitle();
		return this.msgTitle;
	}

	public String getMsgBody(){
		this.msgBody = getMsg(this.msgID).getMessage();
		return this.msgBody;
	}

	public Boolean setPost(int id, String user){
		int i;
		User tmp=null;
		if(user.compareTo(this.username)!=0){
			for(i=0;i<this.friends.length;i++){
				System.out.println(user+"    "+this.friends[i]);
				if(user.compareTo(this.friends[i])==0){
					try{
						tmp = this.server.getUser(this.friends[i]);
					} catch (Exception e){
						return false;
					}
					break;
				}
			}
		}
		else{
			try{
				tmp = this.server.getUser(this.username);
			} catch (Exception e){
				return false;
			}
		}
		if(tmp!=null){
			this.viewPost = tmp.findPost(id);
			return true;
		}
		return false;
	}

	public Post getViewPost(){
		return this.viewPost;
	}

	public void ack(Reply rep) throws java.rmi.RemoteException{
		return;
	}

	public Boolean setPosts(String friend){
		Request req = new Request();
		req.setUsername(username);
		req.setClientInterface(this);
		int i=0;

		if(username==null){
			return false;
		}
		if(friends==null){
			listFriends();
		}
		if(friends==null){
			return false;
		}
		if(friend.compareTo(username)!=0){
			for(i=0;i<this.friends.length;i++){
				if(friends[i].compareTo(friend)==0){
					req.setName(this.friends[i]);
					break;
				}
			}
			if(i==this.friends.length){
				return false;
			}
		}
		else{
			req.setName(username);
		}

		try{
			Reply rep = this.server.consultarPosts(req);
			this.posts = rep.getPosts();
			return true;
		}
		catch (Exception e){
			e.printStackTrace();
			return false;
		}

	}

	public Boolean setAllPosts(){
		this.listFriends();
		int i,k;

		this.allPosts = new ArrayList<Post>();

		Request req = new Request();
		Post []p = null;
		req.setUsername(username);
		req.setClientInterface(this);

		for(i=0;i<this.friends.length;i++){
			req.setName(this.friends[i]);
			try{
				Reply rep = this.server.consultarPosts(req);
				p = rep.getPosts();
				for(k=0;k<p.length;k++){
					allPosts.add(p[k]);
				}
			} catch (Exception e){
				e.printStackTrace();
			}
		}
		return true;
	}

	public ArrayList<Post> getAllPosts(){
		if(this.allPosts==null) setAllPosts();
		Collections.sort(this.allPosts, new DateComparator());
		return this.allPosts;
	}

	public Post[] getPosts(){
		return this.posts;
	}

	public Boolean addPost(String title, String post, File file){
		Request req = new Request();
		req.setUsername(username);
		req.setClientInterface(this);
		if(file!=null){
			byte[] b = new byte[(int) file.length()];
			try{
				b = Files.readAllBytes(file.toPath());
			} catch (Exception e){
				e.printStackTrace();
			}
			req.setImageBytes(b);
		}

		Post p = new Post(title,post,new Date());

		req.setName(this.username);
		req.setPost(p);
		
		try{
			this.server.novoPost(req);
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		
	}

	public Boolean addReply(String user, int id, String reply){
		Request req = new Request();
		req.setUsername(this.username);
		req.setClientInterface(this);
		req.setName(user);
		req.setID(id);
		req.setPass(reply);
		try{
			this.server.responderPost(req);
			return true;
		} catch (Exception e){
			return false;
		}
	}

	public Boolean deletePost(int id){
		Request req = new Request();
		req.setUsername(this.username);
		req.setClientInterface(this);
		req.setID(id);
		try{
			this.server.apagarPost(req);
			return true;
		} catch (Exception e){
			return false;
		}
	}

	public Boolean editPost(int id,String post_title, String post_body){
		Request req = new Request();
		req.setUsername(this.username);
		req.setClientInterface(this);
		Post p = new Post(post_title,post_body,new Date());
		p.setID(id);
		req.setPost(p);
		try{
			this.server.editarPost(req);
			return true;
		} catch (Exception e){
			return false;
		}
	}

	public void updateOnline(String users){
		OnlineWebSocketServlet.sendBroadcast(users);
	}

	public void noti(Reply rep) throws java.rmi.RemoteException{
		System.out.println("TESTE");
		String notification = rep.getReport();
		System.out.println(notification);
		NotificationWebSocketServlet.sendNotification(username + ":" + notification);
	}

	/*DatePosts.sort(list, new Comparator<Post>() {
		public int compare(Post a, Post b) {
			return a.getDate().compareTo(b.getDate());
		}
	});*/

static class DateComparator implements Comparator<Post>{
	public int compare(Post p1, Post p2){
		return p2.getCreated().compareTo(p1.getCreated());
	}
}
}
