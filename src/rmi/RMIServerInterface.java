package rmi;

import db.*;

public interface RMIServerInterface extends java.rmi.Remote{
	public Reply login(Request req) throws java.rmi.RemoteException;
	public void silentLogin(Request req) throws java.rmi.RemoteException;
	public Reply registar(Request req) throws java.rmi.RemoteException;
	public Reply listarAmigos(Request req) throws java.rmi.RemoteException;
	public Reply consultarPosts(Request req) throws java.rmi.RemoteException;
	public void novoPost(Request req) throws java.rmi.RemoteException;
	public void editarPost(Request req) throws java.rmi.RemoteException;
	public void apagarPost(Request req) throws java.rmi.RemoteException;
	public void enviarMensagem(Request req) throws java.rmi.RemoteException;
	public Reply consultarMensagens(Request req) throws java.rmi.RemoteException;
	public void responderPost(Request req) throws java.rmi.RemoteException;
	public Reply adicionarAmigo(Request req) throws java.rmi.RemoteException;
	public void removerAmigo(Request req) throws java.rmi.RemoteException;
	public Reply listOnlineUsers(Request req) throws java.rmi.RemoteException;
	public Reply logout(Request req) throws java.rmi.RemoteException;
	public void subscribe(ClientInterface c) throws java.rmi.RemoteException;
	public User getUser(String user) throws java.rmi.RemoteException;
	public void setFacebookRestClient(Request req) throws java.rmi.RemoteException;
	public void saveFiles() throws java.rmi.RemoteException;
	public void subscribeWS(WebServerInterface ws) throws java.rmi.RemoteException;
}

