package rmi;

import db.Reply;

public interface ClientInterface extends java.rmi.Remote{
	public void noti(Reply rep) throws java.rmi.RemoteException;
	public void ack(Reply rep) throws java.rmi.RemoteException;
}