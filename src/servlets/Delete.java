package servlets;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import bean.Functions;
import db.*;

public class Delete extends HttpServlet{
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
			HttpSession session = request.getSession(true);
			Functions f = (Functions)session.getAttribute("functions");

			if(f==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}
			if(f.getUsername()==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}

			String post_id = request.getParameter("post_id");
			String redirect = request.getParameter("redirect");
			if(redirect==null){
				redirect = "Posts?user="+f.getUsername();
			}
			int id;
			try{
				id = Integer.parseInt(post_id);
			} catch (Exception e){
				response.sendRedirect("/sd/"+redirect);
				return;
			}
			
			Boolean sucess = f.deletePost(id);

			response.sendRedirect("/sd/"+redirect);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}