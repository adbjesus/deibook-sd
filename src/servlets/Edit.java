package servlets;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import bean.Functions;
import db.*;

public class Edit extends HttpServlet{
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
			HttpSession session = request.getSession(true);
			Functions f = (Functions)session.getAttribute("functions");

			if(f==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}
			if(f.getUsername()==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}

			String post_id = request.getParameter("post_id");
			String post_title = request.getParameter("post_title");
			String post_body = request.getParameter("post_body");
			String redirect = request.getParameter("redirect");
			if(redirect==null){
				redirect = "Posts?user="+f.getUsername();
			}
			int id;
			try{
				id = Integer.parseInt(post_id);
			} catch (Exception e){
				response.sendRedirect("/sd/"+redirect);
				return;
			}
			
			Boolean sucess = f.editPost(id,post_title,post_body);

			response.sendRedirect("/sd/"+redirect);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}