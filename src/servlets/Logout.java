package servlets;
import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import bean.*;

public class Logout extends HttpServlet{
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
			HttpSession session = request.getSession(true);
			Functions f = (Functions) session.getAttribute("functions");

			if(f==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}
			if(f.getUsername()==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}
			if(f.logout()){
				f.setUsername(null);
				session.invalidate();
				response.sendRedirect("pages/index.jsp");
				return;
			}
			else {
				response.sendRedirect("pages/main.jsp");
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}