package servlets;

import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import bean.Functions;
import bean.Chatrooms;
import db.*;

public class FacebookLogin extends HttpServlet{
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
	 		HttpSession session = request.getSession(true);
			
			Functions f = (Functions)session.getAttribute("functions");
	 		String code = request.getParameter("code");

	 		FacebookRestClient temp = new FacebookRestClient(false);
	 		System.out.println(code);
	 		temp.verifyToken(code);
	 		f.setFacebookRestClient(temp);
	 		response.sendRedirect("/sd/pages/main.jsp");
		} catch (Exception e) {
			e.printStackTrace();
	 	}
	}
}