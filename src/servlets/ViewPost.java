package servlets;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import bean.Functions;

public class ViewPost extends HttpServlet{
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
			HttpSession session = request.getSession(true);

			Functions f = (Functions)session.getAttribute("functions");

			if(f==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}
			if(f.getUsername()==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}

			String user = request.getParameter("user");
			String id_str = request.getParameter("id");
			int id = -1;

			if(id_str==null || user==null){
				response.sendRedirect("pages/main.jsp");
				return;
			}
			try{
				id = Integer.parseInt(id_str);
			} catch (Exception e){
				response.sendRedirect("pages/main.jsp");
				return;
			}

			Boolean sucess = false;
			
			sucess = f.setPost(id,user);
			if(sucess){
				//GOTO show post page
				response.sendRedirect("pages/view_post.jsp");
				return;
			}
			response.sendRedirect("pages/main.jsp");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
