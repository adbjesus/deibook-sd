package servlets;

import org.apache.catalina.websocket.WebSocketServlet;
import org.apache.catalina.websocket.MessageInbound;
import org.apache.catalina.websocket.StreamInbound;
import org.apache.catalina.websocket.WsOutbound;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import javax.servlet.*;
import javax.servlet.http.*;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.io.IOException;
import java.util.ArrayList;
import java.lang.String;
import bean.Functions;

public class ChatWebSocketServlet extends WebSocketServlet {
	
	private final ArrayList<Chatroom> rooms = new ArrayList<Chatroom>(); 

	protected StreamInbound createWebSocketInbound(String subProtocol,HttpServletRequest request) {
		HttpSession session = request.getSession(true);
		Functions f = (Functions)session.getAttribute("functions");
		String sala=f.getRoom();
		boolean exists = false;
		int i;
		
		//Procurar sala
		for(i=0;i<rooms.size();i++){
			if(rooms.get(i).getRoom().equals(sala)){
				break;
			}
		}

		if(i == rooms.size()){
			//Criar nova sala
			rooms.add(new Chatroom(sala));
		}

		//adicionar uma connection
		return rooms.get(i).createConnection(request);
	}

	private final class ChatMessageInbound extends MessageInbound {
		private final String nickname;
		private final Chatroom room;
		
		private ChatMessageInbound(HttpServletRequest request,Chatroom temp) {
			//Retirar nome e sala
			HttpSession session = request.getSession(true);
			Functions f = (Functions)session.getAttribute("functions");
			this.nickname=f.getUsername();
			this.room = temp;
		}

		protected void onOpen(WsOutbound outbound) {
			this.room.getConnections().add(this);
		}

		protected void onClose(int status) {
			this.room.getConnections().remove(this);
		}

		protected void onTextMessage(CharBuffer message) throws IOException {
			// never trust the client
			String filteredMessage = filter(message.toString());
			broadcast(filteredMessage);
		}


		public String filter(String message) {
			if (message == null)
				return (null);
			// filter characters that are sensitive in HTML
			char content[] = new char[message.length()];
			message.getChars(0, message.length(), content, 0);
			StringBuilder result = new StringBuilder(content.length + 50);
			for (int i = 0; i < content.length; i++) {
				switch (content[i]) {
					case '<':
					result.append("&lt;");
					break;
					case '>':
					result.append("&gt;");
					break;
					case '&':
					result.append("&amp;");
					break;
					case '"':
					result.append("&quot;");
					break;
					default: 
					result.append(content[i]);
				}
			}
			return (result.toString());
		}

		private void broadcast(String message) { // send message to all
			for (ChatMessageInbound connection : this.room.getConnections()) {
				try {
					CharBuffer buffer = CharBuffer.wrap("&lt;" + nickname + "&gt; " + message);
					connection.getWsOutbound().writeTextMessage(buffer);
				} catch (IOException ignore) {}
			}
		}


		protected void onBinaryMessage(ByteBuffer message) throws IOException {
			throw new UnsupportedOperationException("Binary messages not supported.");
		}
	}

	private final class Chatroom{
		private final Set<ChatMessageInbound> connections;
		private final String room;

		public Chatroom(String sala){
			connections =  new CopyOnWriteArraySet<ChatMessageInbound>();
			this.room=sala;
		}

		public String getRoom(){
			return this.room;
		}

		public ChatMessageInbound createConnection(HttpServletRequest request){
			return new ChatMessageInbound(request,this);
		}

		public Set<ChatMessageInbound> getConnections(){
			return this.connections;
		}
	}
}