package servlets;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import bean.Functions;

public class Posts extends HttpServlet{
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
			HttpSession session = request.getSession(true);
			Functions f = (Functions)session.getAttribute("functions");

			if(f==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}
			if(f.getUsername()==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}

			String user = request.getParameter("user");
			Boolean sucess = false;
			
			if(user!=null){
				sucess = f.setPosts(user);
			}
			if(sucess){
						//GOTO show post page
				response.sendRedirect("pages/posts.jsp");
				return;
			}
			sucess = f.setPosts(f.getUsername());
			response.sendRedirect("pages/posts.jsp");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
