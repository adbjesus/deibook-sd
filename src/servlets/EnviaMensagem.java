package servlets;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import bean.Functions;

public class EnviaMensagem extends HttpServlet{
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
			HttpSession session = request.getSession(true);

			Functions f = (Functions)session.getAttribute("functions");
			if(f==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}
			if(f.getUsername()==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}

			String destinatario = request.getParameter("dest_field");
			String assunto= request.getParameter("title_field");
			String corpo = request.getParameter("body_field");
			
			
			f.sendMessage(destinatario,assunto,corpo);
			response.sendRedirect("pages/mensagens.jsp");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		doGet(request,response);
	}
}