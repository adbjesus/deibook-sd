package servlets;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import bean.Functions;
import db.*;

public class Reply extends HttpServlet{
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
			HttpSession session = request.getSession(true);
			Functions f = (Functions)session.getAttribute("functions");

			if(f==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}
			if(f.getUsername()==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}
			Post p = f.getViewPost();
			if(p==null){
				response.sendRedirect("/sd/pages/main.jsp");
				return;
			}

			String reply = request.getParameter("post_reply");
			
			Boolean sucess = f.addReply(p.getUser(),p.getID(),reply);

			response.sendRedirect("/sd/ViewPost?user="+p.getUser()+"&id="+p.getID());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}