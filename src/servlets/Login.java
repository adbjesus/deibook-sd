package servlets;
import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import bean.Functions;
import bean.Chatrooms;
import db.*;

public class Login extends HttpServlet{
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
	 		HttpSession session = request.getSession(true);
			
			Functions f = (Functions)session.getAttribute("functions");
	 		Chatrooms c = (Chatrooms)session.getAttribute("chatrooms");
	 		Boolean active = Boolean.valueOf(request.getParameter("facebook_field"));

	 		if (active != null){
	 			System.out.println(active);
	 		}else{
	 			System.out.println("estou a null!");
	 		}

			if(f==null){
				f = new Functions();
				session.setAttribute("functions",f);
			}
			if(c==null){
				c= new Chatrooms();
				session.setAttribute("chatrooms",c);
			}

			String password = request.getParameter("pass_field");
	 		String username = request.getParameter("username_field");
	 		String facebook = request.getParameter("facebook_login");

	 		if(password==null || username==null){
	 			f.setUsername(null);
				String msg= "Wrong username/password! Please try again...";
	 			session.setAttribute("msg", msg);
	 			response.sendRedirect("pages/notification.jsp");
	 			return;
	 		}

	 		Boolean login = f.login(username,password);			
			
	 		if(login){
				f.setUsername(username);
	 			if(facebook!=null){
	 				FacebookRestClient rest = new FacebookRestClient(false);
	 				String url = rest.getAuthorizationURL();
	 				//url = url.substring(0,url.indexOf("&redirect"))+url.substring(url.indexOf("&scope"))+url.substring(url.indexOf("&redirect"),url.indexOf("&scope"));
	 				System.out.println(url);
	 				response.sendRedirect(url);
	 				return;
	 			}
	 			response.sendRedirect("pages/main.jsp");
	 		}else{
	 			f.setUsername(null);
				String msg= "Wrong username/password! Please try again...";
	 			session.setAttribute("msg", msg);
	 			response.sendRedirect("pages/notification.jsp");
	 		}
		} catch (Exception e) {
			e.printStackTrace();
	 	}
	}
}