package servlets;

import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import bean.Chatrooms;
import bean.Functions;

public class ChatManagement extends HttpServlet{
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
			HttpSession session = request.getSession(true);
			Functions f = (Functions)session.getAttribute("functions");
			
			if(f==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}
			if(f.getUsername()==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}

			String room = request.getParameter("room_field");
			String param=request.getParameter("param_field");

			ServletContext context = session.getServletContext();
			Chatrooms c = (Chatrooms)context.getAttribute("chatrooms");

			
			if(c.hasRoom(room)){
				System.out.println("Juntei a uma sala");
				f.setRoom(room);
			}else{
				System.out.println("Adicionei uma sala");
		 				//create a new room and join
				c.addRoom(room);
				f.setRoom(room);
			}
			response.sendRedirect("pages/chat.jsp");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		doGet(request,response);
	}
}