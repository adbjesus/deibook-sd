package servlets;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

import bean.Functions;

public class AddFriend extends HttpServlet{
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
			HttpSession session = request.getSession(true);
			Functions f = (Functions)session.getAttribute("functions");

			if(f==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}
			if(f.getUsername()==null){
				response.sendRedirect("pages/index.jsp");
				return;
			}

			String friend = request.getParameter("friend_field");
			String msg="";
			
			
			Boolean sucess = f.addFriend(friend);
			
			if(!sucess){
				msg = "Friend not added!";
			}else{
				msg = "User added as a friend!";
			}
			session.setAttribute("msg", msg);
			response.sendRedirect("pages/notification_amigos.jsp");
			
	} catch (Exception e) {
		e.printStackTrace();
	}
}
public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
	doGet(request,response);
}
}