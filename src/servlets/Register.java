package servlets;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

import bean.Functions;


public class Register extends HttpServlet{
	private String msg;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException{
		try{
			HttpSession session = request.getSession(true);
			Functions f = (Functions)session.getAttribute("functions");

			if(f==null){
				f = new Functions();
				session.setAttribute("functions",f);
			}
			if(f.getUsername()!=null){
				f.setUsername(null);
			}

			String username = request.getParameter("username_field");
			String password = request.getParameter("pass_field");
			
			Boolean sucess = f.register(username,password);

			if(!sucess){
				msg = "User "+username+" already exists! You will now be redirected to the main page...";
			}else{
				msg = "User "+username+" registered sucessfully! You will now be redirected to the main page...";
			}
			session.setAttribute("msg", msg);
			response.sendRedirect("pages/notification.jsp");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}