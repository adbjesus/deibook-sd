package servlets;

import org.apache.catalina.websocket.WebSocketServlet;
import org.apache.catalina.websocket.MessageInbound;
import org.apache.catalina.websocket.StreamInbound;
import org.apache.catalina.websocket.WsOutbound;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicInteger;
import java.io.IOException;

import javax.servlet.*;
import javax.servlet.http.*;

import bean.*;

public class OnlineWebSocketServlet extends WebSocketServlet {

	public static final Set<ChatMessageInbound> connections = new CopyOnWriteArraySet<ChatMessageInbound>();

	protected StreamInbound createWebSocketInbound(String subProtocol, HttpServletRequest request) {
		return new ChatMessageInbound(request);
	}

	public static void sendBroadcast(String users){
		for (ChatMessageInbound connection : connections) {
			try {
				CharBuffer buffer = CharBuffer.wrap(users);
				connection.getWsOutbound().writeTextMessage(buffer);
			} catch (IOException ignore) {}
		}
	}

	private class ChatMessageInbound extends MessageInbound {

		private final String nickname;

		private ChatMessageInbound(HttpServletRequest request) {
			HttpSession session = request.getSession(true);
			Functions f = (Functions)session.getAttribute("functions");
			this.nickname=f.getUsername();
		}

		protected void onOpen(WsOutbound outbound) {
			System.out.println("Adicionei uma conexao!");
			connections.add(this);
		}

		protected void onClose(int status) {
			System.out.println("Removi uma conexao!");
			connections.remove(this);
		}

		protected void onTextMessage(CharBuffer message) throws IOException {
			String tmp = message.toString();
			System.out.println(tmp);
			if(tmp.equals("get.users")){
				Chatrooms.updateUsers();
			}
		}

		protected void onBinaryMessage(ByteBuffer message) throws IOException {
			throw new UnsupportedOperationException("Binary messages not supported.");
		}
	}
}