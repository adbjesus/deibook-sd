<jsp:include page="header.jsp"/>
	<%@page import="db.Message"%>
	<%@page import="bean.Functions"%>
	<jsp:include page="sidebar_mensagens.jsp"/>

	<div id="content">
		<ul id="zebra_table">
			<ul id="row" class="title">
				<li id="elem1"> Sender </li>
				<li id="elem2"> Title </li>
				<li id="elem3"> Date </li>
			</ul>
			<%
			Functions f = (Functions) session.getAttribute("functions"); 
			Message[] msgs = f.getMsgs();
			Message temp;
			String ev_od = "even";
			for (int i=0;i<msgs.length;i++){
				if(i%2 == 0){
					ev_od = "even";
				}
				else{
					ev_od = "odd";
				}
			temp= msgs[i];
			out.println("<ul id='row' class='" + ev_od+ "'><a href='/sd/pages/view_messages.jsp?msgID="
			+ Integer.toString(i) + "'><li id='elem1'>"
			+ temp.getSender() + "</li><li id='elem2'>"
			+ temp.getTitle() + "</li><li id='elem3'>"
			+ temp.getDate().toString() 
			+ "</li></a></ul>" );
		}
		%>
	</ul>
</div>

<jsp:include page="footer.jsp"/>
