<jsp:include page="header.jsp"/>
<jsp:include page="sidebar_mensagens.jsp"/>

<div id="content">
	<h1 id="mensagem_noti">Mensagem</h1>
	<div id= "msg_read_from">
		<b>From:</b> ${functions.msgSender}
	</div>
	<div id = "msg_read_title">
		<b>Title:</b> ${functions.msgTitle}
	</div>
	<div id = "msg_read_body">
		<b>Body:</b> 
		<p id= "msg_txt">${functions.msgBody}</p>
	</div>
	
	<form method="get" action="/sd/CheckInbox">
		<input class="button right send" type="submit" value="Back"/>
	</a>
</div>
<jsp:include page="footer.jsp"/>
