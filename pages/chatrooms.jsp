<jsp:include page="header.jsp"/>
	<jsp:include page="sidebar_chatrooms.jsp"/>

	<div id="content">
		<%@page import="bean.Chatrooms"%>
		<%@page import="java.util.ArrayList"%>
		<h1 id = "mensagem_noti">Salas de Chat existentes</h1>
		<ul id="navlist">
			<%
				Chatrooms c = (Chatrooms) application.getAttribute("chatrooms");
				ArrayList <String> salas= c.getRooms();

				if(salas.size()==0){
					out.println("Nao existem salas");
				}else{
					for(int i=0;i<salas.size();i++){
						out.println("<li><a href='/sd/ChatManagement?room_field="+salas.get(i)+"'>"+salas.get(i)+"</a></li>");
					}
				}
			%>
		</ul>
		<div id="new_room">
			<h1>Adicionar Sala</h1>
			<form action="/sd/ChatManagement" method="get">
				<input required name="room_field" type="text" placeholder="Nome da sala"/>
				<input class="button right" type="submit" value="Criar"/>
			</form>
		</div>
	</div>
<jsp:include page="footer.jsp"/>