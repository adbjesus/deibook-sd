<jsp:include page="header.jsp"/>
<%@page import="es.*"%>
<%@page import="java.lang.*"%>

	<jsp:include page="sidebar_profile.jsp"/>
	<div id="content">
		<%!String tipo;%>
		<% Utilizador user = (Utilizador) session.getAttribute("user"); %>
		<% 	if (user instanceof  Admin){
				tipo = "Admin";
			}else if(user instanceof  Cliente){
				tipo = "Cliente";
			}else{
				tipo = "Utilizador";
			}
		%>	
		<p class="detail"> Nome:  	<%=user.getUsername()%></p>
		<p class="detail"> Email: 	<%=user.getEmail()%></p>
		<p class="detail"> Genero: 	<%=user.getGenero()%></p>
		<p class="detail"> Idade: 	<%=String.valueOf((user.getIdade()))%></p>
		<p class="detail"> Tipo: 	<%=tipo%></p>
	</div>
<jsp:include page="footer.jsp"/>
