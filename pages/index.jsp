<jsp:include page="header_front.jsp"/>
	
		<div id="login_form" class="form">
			<h1>Login</h1>
			<form action="/sd/Login" method="post">
				<input name="username_field" type="text" placeholder="Username"/>
				<input name="pass_field" type="password" placeholder="Password"/>
				<input type="checkbox" name="facebook_login" value="facebook" style="height: 15px; width: 15px"/> Facebook Integration
				<input class="button" type="submit" value="Login"/>
			</form>
		</div>
		<div id="register_form" class="form">
			<h1>Register</h1>
			<form action="/sd/Register" method="post">
				<input required name="username_field" type="text" placeholder="Username"/>
				<input required name="pass_field" type="password" placeholder="Password"/>
				<input class="button" type="submit" value="Register"/>
			</form>
		</div>
<jsp:include page="footer_front.jsp"/>
