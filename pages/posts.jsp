<jsp:include page="header.jsp"/>
<%@page import="bean.Functions"%>
<%@page import="db.Post"%>
	<jsp:include page="sidebar_posts.jsp"/>
	<div id="content">
		<h1 id="mensagem_noti">Posts</h1>
		<ul id="navlist">
		<%  
			Functions f = (Functions)session.getAttribute("functions");
			Post []posts = f.getPosts();
			if(posts!=null){
				for(int i=posts.length-1;i>=0;i--){
					out.println("<li><a href='/sd/ViewPost?user="+posts[i].getUser()+"&id="+posts[i].getID()+"'>"+posts[i].getTitle()+"</a>");
					if(posts[i].getUser().compareTo(f.getUsername())==0){
						out.println("<div class='right'>");
						out.println("<a href='/sd/Delete?post_id="+posts[i].getID()+"'>Delete</a>");
						out.println("</div>");
					}
					out.println("</li>");
				}
			}
		%>
		</ul>
	</div>
	

<jsp:include page="footer.jsp"/>
