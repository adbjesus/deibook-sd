<jsp:useBean id="rmi" class="bean.RMIClient" scope="application"/>
<jsp:useBean id="chatrooms" class="bean.Chatrooms" scope="application"/>
<jsp:useBean id="functions" class="bean.Functions" scope="session"/>
<jsp:setProperty name="functions" property="*"/>
<%@page import="bean.Functions"%>
<%
Functions f = (Functions)session.getAttribute("functions");
if(f==null || f.getUsername()==null){
	pageContext.forward("index.jsp");	
}
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="/sd/css/style.css" type="text/css">
	<script type="text/javascript" src="../js/websocket_notifications_friends.js"></script>
	<script type="text/javascript" src="/sd/js/websocket_notifications_friends.js"></script>
</head>
<body>
<div id="container">
	<div id="header" class="grey">
		<div class="centered foot_head">
			<div id="logo" class="left"><a href="/sd/pages/main.jsp">DeiBook</a></div>
			<div id="header_list" class="right">
				<a href="/sd/pages/chatrooms.jsp">Chatrooms</a>
				<a href="/sd/pages/mensagens.jsp">Messages</a>
				<a href="/sd/ListFriends">Friends</a>
				<a href="/sd/Logout">Logout</a>
			</div>
		</div>
	</div>
	<div id="main" class="centered lowered">
		<jsp:include page="sidebar_right.jsp"/>
